# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    Renderer for Paizo forum bbcode
"""

from . import Renderer, option
from .. import inventory


class BBCodeRenderer(Renderer):
    """Paizo forum"""

    @classmethod
    def default_extension(cls):
        return 'bbcode'

    @classmethod
    def extensions(cls):
        return super(BBCodeRenderer, cls).extensions() + ('txt', 'text', 'bbc')

    @property
    def text(self):
        return True

    def __init__(self, file):
        super(BBCodeRenderer, self).__init__(file)
        self._inc_char = True
        self._use_ballots = True

    @option
    def character_details(self):
        """Include character details block"""
        return self._inc_char

    @character_details.setter
    def character_details(self, val):
        self._inc_char = val

    @option
    def use_ballots(self):
        """Use ballot characters instead of plain ascii"""
        return self._use_ballots

    @use_ballots.setter
    def use_ballots(self, val):
        self._use_ballots = val

    def render(self, inv):
        """Render the given inventory."""
        # by default, call start(), entry()..., end()
        if hasattr(self, 'start'):
            self.start(inv)
        for i in (i for i in inv if not i.historical):
            self.entry(i, inv)
        if hasattr(self, 'mid'):
            self.mid(inv)
        for i in (i for i in inv if i.historical):
            self.entry(i, inv)
        if hasattr(self, 'end'):
            self.end(inv)

    def start(self, inv):
        o = self._out
        if self._inc_char:
            o.write(u'Player: [b]%s[/b]\n' % inv.player_name)
            o.write(u'Character: [b]%s[/b]\n' % inv.character_name)
            o.write(u'PFS ID: [b]%d-%d[/b]\n' % inv.pfs_id)
            if inv.faction:
                o.write(u'Faction: [b]%s[/b]\n' % inv.faction)
            o.write(u'\n')
        o.write(u'[spoiler=Inventory]\n[i][b]Item[/b]' +
                u' - Cost Bought/Sold/Expended Charges[/i]\n')

    def mid(self, inv):
        o = self._out
        o.write(u'[/spoiler]\n\n' +
                u'[spolier=Historical Inventory]\n[i][b]Item[/b]' +
                u' - Cost Bought/Sold/Expended Charges[/i]\n')

    def end(self, inv):
        o = self._out
        o.write(u'[/spoiler]\n')

    def item(self, item, inv):
        o = self._out
        o.write(u'[b]%s[/b] - %s %s/%s/%s\n' % (
            item.name,
            _p(item.cost),
            _c(item.bought),
            _c(item.sold),
            _c(item.expended)
            ))

    def consumable(self, item, inv):
        def _x(remain):
            return remain and '-' or 'x'

        def _g(string, group=5):
            return ' '.join(string[i:i+group]
                            for i in range(0, len(string), group))

        o = self._out
        o.write(u'[b]%s[/b] - %s %s/%s/%s ' % (
            item.name,
            _p(item.cost),
            _c(item.bought),
            _c(item.sold),
            _x(item.remain)
            ))
        o.write(_g(
            (self._use_ballots and u'☒' or 'x') * (item.qty - item.remain) +
            (self._use_ballots and u'☐' or '-') * item.remain))
        o.write(u'\n')


# helper functions for above
def _c(val):
    return val and str(val) or '-'


def _p(val):
    try:
        s = ''
        if val[0]:
            s += str(val[0])
        if val[0] and val[1]:
            s += '+'
        if val[1]:
            s += str(val[1]) + 'PP'
        return s
    except TypeError:
        return val and str(val) or '-'
