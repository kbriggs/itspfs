# -*- coding: utf-8 -*-

# Copyright © 2015 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    Renderer for PDF
"""

import re

from . import Renderer, option
from .. import inventory
from ..gui.license import COMMUNITY_USE_HTML

from PySide import QtCore, QtGui
from PySide.QtCore import Qt


COMMUNITY_USE_TEXT = re.sub(r'<a\s+href="([^"]*)"[^>]*>([^<]*)</a>', r'\1',
                            re.sub(r'\bprogram\b', 'character sheet',
                                   COMMUNITY_USE_HTML))


class PdfRenderer(Renderer):
    """Portable document format"""

    @classmethod
    def default_extension(cls):
        return 'pdf'

    class Page:
        MAX_ITEMS = 24
        MAX_CONSUMABLES = 6

        def __init__(self, number):
            self.number = number
            self.items = 0
            self.consumables = 0

        def incitems(self):
            self.items = self.items + 1

        def incconsumables(self):
            self.consumables = self.consumables + 1

    def __init__(self, file):
        super(PdfRenderer, self).__init__(file)
        self._page = None
        self._printer = QtGui.QPrinter(QtGui.QPrinter.HighResolution)
        self._printer.setOutputFormat(QtGui.QPrinter.PdfFormat)
        self._printer.setOutputFileName(self._outopened or self._tempfile())
        self._printer.setPageSize(QtGui.QPrinter.A4)  # TODO: detect US Letter
        self._printer.setOrientation(QtGui.QPrinter.Portrait)

    def start(self, inv):
        self._painter = QtGui.QPainter(self._printer)
        self._painter.setRenderHint(QtGui.QPainter.Antialiasing)

        # set up coordinate system in mm
        def _r(unit):
            r = self._printer.pageRect(unit)
            r.moveTo(0, 0)
            return r.toRect()
        self._painter.setViewport(_r(QtGui.QPrinter.DevicePixel))
        self._painter.setWindow(_r(QtGui.QPrinter.Millimeter))
        self._painter.setFont(QtGui.QFont("Verdana"))
        self._startpage(inv)

    def end(self, inv):
        self._painter.end()

    def _tempfile(self):
        from tempfile import NamedTemporaryFile
        self._outtemp = NamedTemporaryFile()
        return self._outtemp.name

    def _pen(self, mm, c=QtGui.QColor('black')):
        p = self._painter.pen()
        p.setWidthF(mm)
        p.setBrush(c)
        self._painter.setPen(p)

    def _fontsize(self, pt, w=QtGui.QFont.Normal):
        f = self._painter.font()
        ret = (pt * (
               self._printer.paperRect(QtGui.QPrinter.Millimeter).height() /
               self._printer.paperRect(QtGui.QPrinter.DevicePixel).height()))
        f.setPointSizeF(ret)
        f.setWeight(w)
        self._painter.setFont(f)
        return ret

    def _centertext(self, x, y, s):
        c = x - (self._painter.fontMetrics().width(s) / 2)
        self._painter.drawText(c, y, s)

    def _righttext(self, x, y, s):
        c = x - (self._painter.fontMetrics().width(s))
        self._painter.drawText(c, y, s)

    def _lefttext(self, x, y, s):
        # for symmetry
        self._painter.drawText(x, y, s)

    def _wraptext(self, x, y, w, h, s):
        if self._painter.fontMetrics().width(s) <= w:
            self._lefttext(x, y, s)
        else:
            # fixme: fix the self._rowheight reference to be based on font metrics instead
            self._painter.drawText(QtCore.QRectF(x,
                                                 y - h + 0.2 * self._rowheight,
                                                 w, h),
                                   (Qt.AlignLeft | Qt.AlignBottom |
                                    Qt.TextWordWrap),
                                   s)

    # Here be monsters: lots of ugly  magic numbers arrived at by trial and error coming up.

    def item(self, item, inv):
        if self._page.items >= self._page.MAX_ITEMS:
            self._startpage(inv)
        self._pen(0.5)
        self._fontsize(9)
        colwidth = self._boundary.width() / 2
        x = self._boundary.left() + 2
        y = self._boundary.top() + self._rowheight * (3.6 + self._page.items)
        if self._page.items >= self._page.MAX_ITEMS // 2:
            x += colwidth
            y -= self._page.MAX_ITEMS // 2 * self._rowheight

        self._wraptext(x, y, colwidth / 2 - 4, self._rowheight, item.name)
        self._lefttext(x + colwidth / 2, y, _p(item.cost))
        self._centertext(x + colwidth * 9 / 12, y, _c(item.bought))
        self._centertext(x + colwidth * 10 / 12, y, _c(item.sold))
        self._centertext(x + colwidth * 11 / 12, y, _c(item.expended))
        self._page.incitems()

    def consumable(self, item, inv):
        if self._page.consumables >= self._page.MAX_CONSUMABLES:
            self._startpage(inv)
        self._pen(0.5)
        self._fontsize(9)
        colwidth = self._boundary.width()
        x = self._boundary.left() + 2
        y = (self._boundary.top() +
             self._rowheight * (4.6 +
                                (self._page.MAX_ITEMS // 2) +
                                self._page.consumables)
             )

        self._wraptext(x, y, colwidth / 2 - 4, self._rowheight, item.name)
        self._lefttext(x + colwidth / 4, y, _p(item.cost))
        self._centertext(x + colwidth * 19 / 48 - 4, y, _c(item.bought))
        self._centertext(x + colwidth * 21 / 48 - 4, y, _c(item.sold))
        s = u'\u2611' * (item.qty - item.remain) + u'\u2610' * item.remain
        self._fontsize(10)
        w = colwidth / 2 + 8
        self._chargerect = self._painter.drawText(
            QtCore.QRectF(self._boundary.right() - w - 1,
                          y - 0.7 * self._rowheight,
                          w,
                          self._rowheight),
            (Qt.AlignLeft | Qt.AlignVCenter),
            u'\n'.join(
                u' '.join(s[i:i+5] for i in range(j, j+45, 10))
                for j in range(0, 10, 5)))
        self._page.incconsumables()

    def _startpage(self, inv):
        "Draw the page borders, logo etc"

        # start a new page
        if self._page is None:
            self._page = self.Page(1)
        else:
            self._page = self.Page(self._page.number + 1)
            self._printer.newPage()

        # draw the logo in the upper left
        logo = get_logo()
        if logo:
            lsize = logo.size()
            lsize.scale(100, 50, Qt.KeepAspectRatio)
            lrect = QtCore.QRect(QtCore.QPoint(0, 0), lsize)
            self._painter.drawImage(lrect, logo)
        else:
            lsize = QtCore.QSize(0, 50)

        # draw the title and sheet number in the upper right
        self._fontsize(18, QtGui.QFont.Bold)
        c = ((self._printer.pageRect(QtGui.QPrinter.Millimeter).width() +
              lsize.width()
              ) / 2)
        self._centertext(c, lsize.height()/3,
                         'Inventory Tracking Sheet')
        self._fontsize(12)
        self._righttext(c, lsize.height()*2/3,
                        'Tracking Sheet # ')
        r = QtCore.QRect(c, (lsize.height()*2/3),
                         30, self._painter.fontMetrics().height()+2)
        r.translate(0, 2-r.height())
        self._pen(0.5)
        self._painter.drawRect(r)
        self._fontsize(16)
        self._righttext(c+28, (lsize.height()*2/3), str(self._page.number))

        # Draw the fancy page border
        self._pen(0.1)
        offset = QtCore.QSize(0, lsize.height())
        bsize = self._printer.pageRect(QtGui.QPrinter.Millimeter).size() - offset
        self._painter.drawPath(get_border_path(bsize).translated(*offset.toTuple()))
        self._boundary = QtCore.QRectF(QtCore.QPointF(*offset.toTuple()), bsize)
        self._boundary.translate(0, 5)
        self._rowheight = ((bsize.height() - 10) //
                           (self._page.MAX_ITEMS // 2 + self._page.MAX_CONSUMABLES + 4))

        # Draw the community use policy
        self._fontsize(6)
        self._painter.drawText(QtCore.QRectF(QtCore.QPoint(5, 0),
                                             self._printer.pageRect(QtGui.QPrinter.Millimeter).size() -
                                             QtCore.QSize(10, 0)),
                               (Qt.AlignHCenter | Qt.AlignBottom | Qt.TextWordWrap),
                               COMMUNITY_USE_TEXT)

        # Draw the character/player details section
        self._fontsize(9)
        y = self._boundary.top() + 1.5*self._rowheight
        z = []
        # Player, Character
        sep = 'AKA'
        sepw = self._painter.fontMetrics().width(sep)
        x = (2, 2 + (self._boundary.width() - 14) * 10 / 16)
        c = (x[0] + (x[1] - x[0]) / 2)
        x2 = (c - sepw / 2.0 - 1, c + sepw / 2.0 + 1)
        c += 1
        self._painter.drawLine(
            self._boundary.topLeft()+QtCore.QPointF(x[0], self._rowheight),
            self._boundary.topLeft()+QtCore.QPointF(x2[0], self._rowheight))
        self._painter.drawLine(
            self._boundary.topLeft()+QtCore.QPointF(x2[1], self._rowheight),
            self._boundary.topLeft()+QtCore.QPointF(x[1], self._rowheight))
        self._centertext(c, self._boundary.top()+self._rowheight, sep)
        self._centertext((x[0]+x2[0])/2, y, 'Player Name')
        self._centertext((x[1]+x2[1])/2, y, 'Character Name')
        z += (x[0], x2[1])
        # PFS ID
        sep = u'\u2014'
        sepw = self._painter.fontMetrics().width(sep)
        x = (7 + (self._boundary.width() - 14) * 10 / 16,
             7 + (self._boundary.width() - 14) * 13 / 16)
        c = (x[0] + (x[1] - x[0]) * 2 / 3)
        x2 = (c - sepw / 2.0 - 1, c + sepw / 2.0 + 1)
        c += 1
        self._painter.drawLine(
            self._boundary.topLeft()+QtCore.QPointF(x[0], self._rowheight),
            self._boundary.topLeft()+QtCore.QPointF(x2[0], self._rowheight))
        self._painter.drawLine(
            self._boundary.topLeft()+QtCore.QPointF(x2[1], self._rowheight),
            self._boundary.topLeft()+QtCore.QPointF(x[1], self._rowheight))
        self._centertext(c, self._boundary.top()+self._rowheight, sep)
        self._centertext((x[0]+x[1])/2, y, 'Pathfinder Society #')
        z += (x2[0], x[1])
        # Faction
        x = (self._boundary.width() - 14) * 3 / 16
        self._painter.drawLine(
            self._boundary.topRight()+QtCore.QPointF(-2 - x, self._rowheight),
            self._boundary.topRight()+QtCore.QPointF(-2, self._rowheight))
        self._centertext(self._boundary.right()-2-(x/2), y, 'Faction')
        z += (self._boundary.right()-2-x,)
        # Fill in the actual value
        self._fontsize(14)
        y = self._boundary.top() + self._rowheight - 1
        self._lefttext(z[0], y, inv.player_name)
        self._lefttext(z[1], y, inv.character_name)
        self._righttext(z[2], y, str(inv.pfs_id[0]))
        self._righttext(z[3], y, str(inv.pfs_id[1]))
        self._lefttext(z[4], y, inv.faction)

        # Draw the items section heading
        self._painter.fillRect(
            QtCore.QRectF(0, 0, self._boundary.width(), 1.5)
            .translated(self._boundary.topLeft())
            .translated(0, 2 * self._rowheight),
            QtGui.QColor('black'))
        colwidth = self._boundary.width() / 2
        for i in (0, 1):
            # two columns
            x = (self._boundary.left() + 2 +
                 (i and colwidth))
            y = self._boundary.top() + 2.9 * self._rowheight
            self._fontsize(9, QtGui.QFont.Bold)
            self._lefttext(x, y, 'Item')
            self._lefttext(x + colwidth / 2, y, 'Cost')
            self._centertext(x + colwidth * 5 / 6, y - 0.3 * self._rowheight, 'Chronicle #')
            self._fontsize(7)
            self._centertext(x + colwidth * 9 / 12, y + 0.1 * self._rowheight, 'Bought')
            self._centertext(x + colwidth * 10 / 12, y + 0.1 * self._rowheight, 'Sold')
            self._centertext(x + colwidth * 11 / 12, y + 0.1 * self._rowheight, 'Expended')

        # Draw the empty underlines and boxes for items
        def _blanki(x, y):
            self._painter.drawLine(x, y, x + colwidth / 2 - 2, y)
            self._painter.drawLine(x + colwidth / 2, y, x + colwidth * 2 / 3 - 2, y)
            sz = QtCore.QSize(0.95 * colwidth / 12, 0.7 * self._rowheight)
            path = get_box_path(sz).translated(-sz.width() / 2, -0.05 * self._rowheight)
            self._painter.drawPath(path.translated(x + colwidth * 9 / 12 - 1, y))
            self._painter.drawPath(path.translated(x + colwidth * 10 / 12 - 0.5, y))
            self._painter.drawPath(path.translated(x + colwidth * 11 / 12 - 0, y))
        for j in (0, 1):
            for i in range(0, self._page.MAX_ITEMS // 2):
                _blanki(self._boundary.left() + 2 + (j and colwidth),
                        self._boundary.top() + (3.8 + i) * self._rowheight)

        # Draw the consumables section heading
        self._painter.fillRect(
            QtCore.QRectF(0, 0, self._boundary.width(), 1.5)
            .translated(self._boundary.topLeft())
            .translated(0, (3 + self._page.MAX_ITEMS // 2) * self._rowheight),
            QtGui.QColor('black'))
        x = self._boundary.left() + 2
        y = self._boundary.top() + (3.9 + self._page.MAX_ITEMS // 2) * self._rowheight
        colwidth = self._boundary.width()
        self._fontsize(9, QtGui.QFont.Bold)
        self._lefttext(x, y, 'Item')
        self._lefttext(x + colwidth / 4, y, 'Cost')
        _co = 4
        self._centertext(x + colwidth * 5 / 12 - _co,
                         y - 0.3 * self._rowheight,
                         'Chronicle #')
        self._fontsize(7)
        self._centertext(x + colwidth * 19 / 48 - _co,
                         y + 0.1 * self._rowheight,
                         'Bought')
        self._centertext(x + colwidth * 21 / 48 - _co,
                         y + 0.1 * self._rowheight,
                         'Sold')

        # Draw the empty underlines and boxes for consumables
        def _blankc(x, y):
            self._painter.drawLine(x, y, x + colwidth / 4 - 2, y)
            self._painter.drawLine(x + colwidth / 4, y,
                                   x + colwidth * 2 / 6 - 2, y)
            sz = QtCore.QSize(0.95 * colwidth / 24, 0.7 * self._rowheight)
            path = get_box_path(sz).translated(-sz.width() / 2,
                                               -0.05 * self._rowheight)
            self._painter.drawPath(path.translated(
                x + colwidth * 19 / 48 - _co - 0.5, y))
            self._painter.drawPath(path.translated(
                x + colwidth * 21 / 48 - _co, y))
            self._pen(0.1, QtGui.QColor('lightgrey'))
            self._fontsize(10)
            w = colwidth / 2 + 8
            self._chargerect = self._painter.drawText(
                QtCore.QRectF(self._boundary.right() - w - 1,
                              y - 0.9 * self._rowheight,
                              w,
                              self._rowheight),
                (Qt.AlignLeft | Qt.AlignVCenter),
                u'\n'.join(
                    u' '.join(
                        u'\u2610' * 5
                        for i in range(0, 5))
                    for j in range(0, 2)))
            self._pen(0.1)
        for i in range(0, self._page.MAX_CONSUMABLES):
            _blankc(self._boundary.left() + 2,
                    self._boundary.top() +
                    (4.8 + self._page.MAX_ITEMS // 2 + i) *
                    self._rowheight)


def get_logo():
    if not hasattr(get_logo, '_logo'):
        for ext in ['tif', 'png']:
            try:
                import pkg_resources
                setattr(get_logo, '_logo',
                        QtGui.QImage.fromData(
                            pkg_resources.resource_string('itspfs',
                                                          'images/logo/Pathfinder Society.%s' % ext)))
                break
            except IOError:
                try:
                    with open('images/logo/Pathfinder Society.%s' % ext, 'rb') as f:
                        setattr(get_logo, '_logo', QtGui.QImage.fromData(f.read()))
                    break
                except IOError:
                    pass
    return getattr(get_logo, '_logo', None)


def get_border_path(sz, c=5):
    if not hasattr(get_border_path, '_border_path'):
        (w, h) = sz.toTuple()
        (c1, c2) = (c/3, c*2/3)
        path = QtGui.QPainterPath(QtCore.QPointF(c, 0))
        for a in ((w-c, 0), (w-c, c2), (w-c1, c2), (w-c1, c1),
                  (w-c2, c1), (w-c2, c), (w, c),
                  (w, h-c), (w-c2, h-c), (w-c2, h-c1), (w-c1, h-c1),
                  (w-c1, h-c2), (w-c, h-c2), (w-c, h),
                  (c, h), (c, h-c2), (c1, h-c2), (c1, h-c1),
                  (c2, h-c1), (c2, h-c), (0, h-c),
                  (0, c), (c2, c), (c2, c1), (c1, c1),
                  (c1, c2), (c, c2), (c, 0)):
            path.lineTo(*a)
        setattr(get_border_path, '_border_path', path)
    return getattr(get_border_path, '_border_path', None)


def get_box_path(sz):
    if not hasattr(get_box_path, '_box_path'):
        (w, h) = sz.toTuple()
        path = QtGui.QPainterPath(QtCore.QPointF(0, 0))
        for a in ((w, 0), (w, -h), (0, -h), (0, 0)):
            path.lineTo(*a)
        setattr(get_box_path, '_box_path', path)
    return getattr(get_box_path, '_box_path', None)


# helper functions for above
def _c(val):
    return val and str(val) or ''


def _p(val):
    try:
        s = ''
        if val[0]:
            s += str(val[0])
        if val[0] and val[1]:
            s += '+'
        if val[1]:
            s += str(val[1]) + 'PP'
        return s
    except TypeError:
        return val and str(val) or ''
