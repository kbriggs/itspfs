# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    Package for output rendering
"""

from abc import ABCMeta, abstractmethod, abstractproperty
from six import add_metaclass
from .. import inventory


class option(property):
    """An option for the renderer"""


@add_metaclass(ABCMeta)
class Renderer(object):

    @classmethod
    @abstractmethod
    def default_extension(cls):
        """The default file extension for this renderer."""

    @classmethod
    def extensions(cls):
        """All valid file extensions for this renderer."""
        return (cls.default_extension(),)

    @property
    def text(self):
        return False

    @classmethod
    def options(cls):
        """List the available options for the renderer."""
        for name in cls.__dict__:
            opt = cls.__dict__[name]
            if isinstance(opt, option):
                yield (name, opt)

    def __init__(self, file):
        from six import string_types
        if isinstance(file, string_types):
            self._outstream = open(file, self.text and 'w' or 'wb')
            self._outopened = file
        else:
            self._outstream = file

    def render(self, inv):
        """Render the given inventory."""
        # by default, call start(), entry()..., end()
        if hasattr(self, 'start'):
            self.start(inv)
        for i in inv:
            self.entry(i, inv)
        if hasattr(self, 'end'):
            self.end(inv)

    def entry(self, item, inv):
        if isinstance(item, inventory.Consumable):
            self.consumable(item, inv)
        else:
            self.item(item, inv)

# optional abstract methods, required unless entry() is overridden
#    @abstractmethod
#    def item(self, item, inv):
#        pass
#
#    @abstractmethod
#    def consumable(self, item, inv):
#        pass

    def close(self):
        """Close the underlying output stream."""
        self._out.close()

    # context manager support
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if self._outopened:
            self.close()

    @property
    def _out(self):
        return self._outstream


def renderers():
    """Returns all available concrete Renderer subclasses"""
    from pkgutil import iter_modules
    from inspect import getmembers, isclass, isabstract
    for (loader, modname, ispkg) in iter_modules(__path__, __name__+'.'):
        module = loader.find_module(modname).load_module(modname)
        for o in getmembers(module):
            if (isclass(o[1]) and
                    issubclass(o[1], Renderer) and
                    not isabstract(o[1])):
                yield o[1]
