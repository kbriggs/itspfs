# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

"""
    Inventory model classes
"""


def _isstr(obj):
    from six import string_types
    return isinstance(obj, string_types)


class Inventory(object):
    """
        The main inventory. Defines the character and player details,
        contains the inventory items, and allows serialization to and
        from files.
    """
    def __init__(self, **kwargs):
        self._player_name = 'player' in kwargs and kwargs['player'] or None
        self._character_name = 'name' in kwargs and kwargs['name'] or None
        self._pfs_id = ('pfs' in kwargs and
                        self._split_pfs_id(kwargs['pfs']) or
                        (None, None))
        self._faction = 'faction' in kwargs and kwargs['faction'] or None
        self._items = ('inv' in kwargs and
                       [BaseItem.load(i) for i in kwargs['inv']] or
                       list())

    @property
    def player_name(self):
        return self._player_name

    @property
    def character_name(self):
        return self._character_name

    @property
    def pfs_id(self):
        """A tuple of the player's PFS ID, character's sequence number."""
        return self._pfs_id

    @property
    def faction(self):
        return self._faction

    def save(self, out):
        """Save the character's inventory to a file."""
        def _u23(data):
            try:
                return unicode(data)  # python 2
            except NameError:
                return data  # python 3

        if _isstr(out):
            with open(out, 'w') as f:
                return self.save(f)
        # top level of JSON
        char = {
            'player': self._player_name,
            'name': self._character_name,
            'pfs': self._join_pfs_id(self._pfs_id),
            }
        if self._faction:
            char['faction'] = self._faction
        out.write(_u23(
            json.dumps(char, sort_keys=True, ensure_ascii=False))[:-1])

        # inventory items
        out.write(u', "inv": [\n')
        first = True
        for item in self:
            out.write(first and u'  ' or u', ')
            first = False
            out.write(_u23(
                json.dumps(item,
                           cls=Item._JSONEncoder,
                           sort_keys=True,
                           ensure_ascii=False)))
            out.write(u'\n')

        # end
        out.write(u']}\n')

    @classmethod
    def load(cls, file):
        """Load a character's inventory from a file."""
        if _isstr(file):
            if file.lstrip().startswith('{'):
                # string is direct JSON, read
                d = json.loads(file)
            else:
                # filename, open and load
                with open(file, 'r') as f:
                    return cls.load(f)
        else:
            d = json.load(file)
        return cls(**d)

    @staticmethod
    def _split_pfs_id(id):
        if _isstr(id):
            import re
            m = re.match(r'^\s*#?(\d+)\s*-\s*(\d+)\s*$', id)
            if m:
                return (int(m.group(1)), int(m.group(2)))
            raise ValueError('Invalid PFS ID string')
        elif len(id) == 2:
            return id
        else:
            raise TypeError('PFS ID must be string or pair')

    @staticmethod
    def _join_pfs_id(id):
        if _isstr(id):
            return id
        elif len(id) == 2:
            return '%d-%d' % id
        else:
            raise TypeError('PFS ID must be string or pair')

    # Inventory acts like a sequence of items
    def __len__(self):
        return self._items.__len__()

    def __iadd__(self, item):
        self._items.__iadd__(
            (hasattr(item, '__iter__') or
             hasattr(item, '__getitem__')) and
            item or [item])
        return self

    def __getitem__(self, key):
        return self._items.__getitem__(key)


class BaseItem(object):
    """
        An item. Each item has a name, cost and the chronicle numbers
        on which it was bought and sold.
    """
    def __init__(self, name, cost, bought):
        self._name = name
        self._cost = cost
        try:
            if not self._cost[1]:
                self._cost = self._cost[0]
        except TypeError:
            pass
        self._bought = bought
        self._sold = None

    @property
    def name(self):
        return self._name

    @property
    def cost(self):
        """
            Either a number representing the cost in gold, or a tuple
            of the cost in gold, cost in prestige
        """
        return self._cost

    @property
    def bought(self):
        """Character chronicle number on which the item was bought."""
        return self._bought

    @property
    def sold(self):
        """Character chronicle number on which the item was sold."""
        return self._sold

    @sold.setter
    def sold(self, value):
        self._sold = value

    @property
    def historical(self):
        """Whether the item is no longer owned."""
        return self._sold and True or False

    class _JSONEncoder(json.JSONEncoder):
        def default(self, item):
            if isinstance(item, BaseItem):
                return {k[1:]: v for k, v in item.__dict__.items()
                        if v is not None}
            return super(json.JSONEncoder, self).default(item)

    @classmethod
    def load(cls, item):
        """Load item data from dictionary."""
        if isinstance(item, cls):
            return item

        def _cost(v):
            return isinstance(v, list) and tuple(v) or v

        if 'qty' in item:
            i = Consumable(item['name'], _cost(item['cost']),
                           item['bought'], item['qty'])
            if 'remain' in item:
                i._remain = item['remain']
        else:
            i = Item(item['name'], _cost(item['cost']), item['bought'])
            if 'expended' in item:
                i._expended = item['expended']
        if 'sold' in item:
            i._sold = item['sold']
        return i

    def __eq__(self, other):
        if not isinstance(self, BaseItem):
            return NotImplemented
        return self.__dict__ == other.__dict__


class Item(BaseItem):
    """
        A permanent item.
    """
    def __init__(self, name, cost, bought):
        super(Item, self).__init__(name, cost, bought)
        self._expended = None

    @property
    def expended(self):
        """Character chronicle number on which the item was expended."""
        return self._expended

    @expended.setter
    def expended(self, value):
        self._expended = value

    @property
    def historical(self):
        return super(Item, self).historical or self._expended and True or False


class Consumable(BaseItem):
    """
        A consumable item, which has a quantity associated with it
        (for ammunition, wand charges, alchemical items, etc).
    """
    def __init__(self, name, cost, bought, qty):
        super(Consumable, self).__init__(name, cost, bought)
        self._qty = qty
        self._remain = qty

    @property
    def qty(self):
        return self._qty

    @property
    def remain(self):
        return self._remain

    def __isub__(self, n):
        """Expend the given number of charges."""
        self._remain -= n
        return self

    @property
    def historical(self):
        return super(Consumable, self).historical or self._remain == 0
