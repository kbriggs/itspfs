# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    Custom/compound view components
"""

import re
import os

from PySide import QtCore
from PySide import QtGui
from PySide.QtCore import Slot, Signal

from ..inventory import Inventory, Item, Consumable


class AbstractInventoryWidget(QtGui.QWidget):
    """Abstract superclass for inventory widgets"""
    def __init__(self, inventory=None, parent=None):
        super(AbstractInventoryWidget, self).__init__(parent)
        self._inventory = inventory

    @property
    def inventory(self):
        return self._inventory


class CharacterDetailsWidget(AbstractInventoryWidget):
    """The character details display area"""

    characterNameChanged = Signal(str)
    characterDetailsChanged = Signal(str)

    def __init__(self, inventory=None, parent=None):
        super(CharacterDetailsWidget, self).__init__(inventory, parent)
        self._layout = QtGui.QHBoxLayout()
        self._playername = QtGui.QLineEdit()
        self._charname = QtGui.QLineEdit()
        self._playerpfs = QtGui.QLineEdit()
        self._charpfs = QtGui.QLineEdit()
        self._faction = QtGui.QLineEdit()
        self._playername.setPlaceholderText('Player Name')
        self._charname.setPlaceholderText('Character Name')
        self._faction.setPlaceholderText('Faction')
        self._layout.addWidget(self._playername, stretch=5)
        self._layout.addWidget(QtGui.QLabel('aka'))
        self._layout.addWidget(self._charname, stretch=5)
        self._layout.addSpacing(15)
        self._layout.addWidget(QtGui.QLabel('PFS #'))
        self._layout.addWidget(self._playerpfs, stretch=3)
        self._layout.addWidget(QtGui.QLabel('-'))
        self._layout.addWidget(self._charpfs, stretch=1)
        self._layout.addSpacing(15)
        self._layout.addWidget(self._faction, stretch=4)
        self.setLayout(self._layout)
        if inventory is not None:
            self._playername.setText(inventory.player_name)
            self._charname.setText(inventory.character_name)
            self._playerpfs.setText(str(inventory.pfs_id[0]))
            self._charpfs.setText(str(inventory.pfs_id[1]))
            self._faction.setText(inventory.faction)
            # details cannot be changed once the character is saved
            for w in self._widgets():
                w.setReadOnly(True)
        else:
            self.initSignals()

    def _widgets(self):
        return (self._playername, self._charname,
                self._playerpfs, self._charpfs,
                self._faction)

    def initSignals(self):
        # changing the name
        self._charname.textChanged.connect(self.characterNameChanged)
        # changing the details
        widgets = self._widgets()
        for w in widgets:
            w.textEdited.connect(self.characterDetailsChanged)
        # focus flow
        for i in range(len(widgets)-1):
            widgets[i].returnPressed.connect(getattr(widgets[i+1], 'setFocus'))

    @property
    def details(self):
        return {
            'player': self._playername.text(),
            'name': self._charname.text(),
            'faction': self._faction.text(),
            'pfs': Inventory._join_pfs_id(
                (int(self._playerpfs.text()), int(self._charpfs.text())))
        }

    @property
    def name(self):
        return self._charname.text()


class InventoryGridWidget(AbstractInventoryWidget):
    """The inventory list display area"""

    dirty = Signal()

    def __init__(self, inventory=None, parent=None):
        super(InventoryGridWidget, self).__init__(inventory, parent)
        self._newnamefield = None

        # signals
        parent.beforeSave.connect(self.flush)

        self._layout = QtGui.QGridLayout()
        self._layout.setSpacing(2)
        # headings
        self._layout.addWidget(QtGui.QLabel('Item'), 0, 0, 2, 1)
        self._layout.addWidget(QtGui.QLabel('Cost'), 0, 1, 2, 1)
        self._layout.addWidget(QtGui.QLabel('Chronicle'), 0, 2, 1, 3)
        self._layout.addWidget(QtGui.QLabel('Charges'), 0, 5, 2, 30)
        self._layout.addWidget(QtGui.QLabel('Bought'), 1, 2)
        self._layout.addWidget(QtGui.QLabel('Sold'), 1, 3)
        self._layout.addWidget(QtGui.QLabel('Expended'), 1, 4)
        # tweaks and stretchiness
        font = self._layout.itemAtPosition(1, 2).widget().font()
        fm = QtGui.QFontMetrics(font)
        self._layout.setColumnStretch(0, 10)
        self._layout.setColumnStretch(1, 3)
        self._layout.setColumnStretch(2, 1)
        self._layout.setColumnStretch(3, 1)
        self._layout.setColumnStretch(4, 1)
        self._layout.setColumnMinimumWidth(1, fm.maxWidth()*2)
        self._layout.setColumnMinimumWidth(2, fm.maxWidth())
        self._layout.setColumnMinimumWidth(3, fm.maxWidth())
        self._layout.setColumnMinimumWidth(4, fm.maxWidth())
        small = QtGui.QFont(font)
        small.setPointSize(small.pointSize()//3*2)
        self._layout.itemAtPosition(1, 2).widget().setFont(small)
        self._layout.itemAtPosition(1, 3).widget().setFont(small)
        self._layout.itemAtPosition(1, 4).widget().setFont(small)
        # empty spacer row to prevent row stretch
        self._layout.addItem(QtGui.QSpacerItem(0, 0),
                             self._layout.rowCount(), 0, 1, 35)
        self._layout.setRowStretch(self._layout.rowCount()-1, 1)

        # render initial controls
        if self.inventory is not None:
            self.render()
        self.addBlank()
        self.setLayout(self._layout)

    def moveSpacer(self, rows=1):
        """Shift the spacer row down to make room for actual rows"""
        base = self._layout.rowCount()-1
        self._layout.setRowStretch(self._layout.rowCount()-1, 0)
        spacer = self._layout.itemAtPosition(base, 0)
        self._layout.removeItem(spacer)
        self._layout.addItem(spacer, base+2*rows, 0, 1, 35)
        self._layout.setRowStretch(self._layout.rowCount()-1, 1)
        return base

    def capply(self, rbase, f, max=50, min=0):
        """Apply a function to a range of consumable checkboxes"""
        for j in range(2):
            for i in range(25):
                # TODO: fill by 5-group
                x = j*25+i
                if x >= max:
                    return
                if x >= min:
                    f((self._layout.itemAtPosition((rbase+j),
                                                   (5+i+(i//5)))
                       .widget()),
                      x)

    def ccount(self, rbase, max=50, min=0):
        """Count the checked consumable checkboxes"""
        c = {QtCore.Qt.Unchecked: 0,
             QtCore.Qt.Checked: 0,
             QtCore.Qt.PartiallyChecked: 0}
        for j in range(2):
            for i in range(25):
                # TODO: fill by 5-group
                x = j*25+i
                if x >= max:
                    return
                if x >= min:
                    s = (self._layout.itemAtPosition((rbase+j),
                                                     (5+i+(i//5)))
                         .widget().checkState())
                    c[s] = c[s] + 1
        return c

    def QLineEdit(self, *args, **kwargs):
        """Construct a QtGui.QLineEdit that emits dirty when changed"""
        edit = QtGui.QLineEdit(*args, **kwargs)
        edit.textEdited.connect(self.dirty)
        return edit

    def render(self):
        """Populate the grid from the inventory data"""
        # Helper functions
        from numbers import Number

        def addLineEdit(grid, text, row, col, rowspan=1, colspan=1):
            """Create a new QLineEdit with the given text"""
            w = self.QLineEdit()
            w.setText(text or '')
            w.setEnabled(text is None or text is False)
            grid._layout.addWidget(w, row, col, rowspan, colspan)

        base = self.moveSpacer(len(self.inventory))
        for row in range(len(self.inventory)):
            rbase = base+2*row
            item = self.inventory[row]
            addLineEdit(self, item.name, rbase, 0, 2, 1)
            addLineEdit(self,
                        (isinstance(item.cost, Number) and
                         str(item.cost) or
                         isinstance(item.cost, (list, tuple)) and
                         (str(item.cost[0])+('P'*item.cost[1])) or
                         ''),
                        rbase, 1, 2, 1)
            addLineEdit(self, str(item.bought), rbase, 2, 2, 1)
            addLineEdit(self,
                        item.historical and (item.sold is not None and
                                             str(item.sold) or
                                             ''),
                        rbase, 3, 2, 1)

            if isinstance(item, Consumable):
                c = QtGui.QCheckBox()
                c.setChecked(item.remain == 0)
                c.setEnabled(item.remain > 0)
                self._layout.addWidget(c, rbase, 4, 2, 1)
                for j in range(2):
                    for i in range(25):
                        c = QtGui.QCheckBox()
                        c.setEnabled(False)
                        c.setTristate(True)
                        self._layout.addWidget(c, (rbase+j), (5+i+(i//5)))
                    for i in range(4):
                        self._layout.addItem(QtGui.QSpacerItem(10, 10),
                                             (rbase+j), (i*6+10))
                self.capply(
                    rbase,
                    lambda c, x: c.setObjectName('charge_%d' % (x)))
                self.capply(
                    rbase,
                    lambda c, x: c.setChecked(True),
                    item.qty - item.remain)
                self.capply(
                    rbase,
                    lambda c, x: c.setEnabled(True),
                    item.qty,
                    item.qty - item.remain)
                self.capply(
                    rbase,
                    lambda c, x: c.clicked.connect(self.chargeClicked),
                    item.qty,
                    item.qty - item.remain)
                self.capply(
                    rbase,
                    lambda c, x: c.setCheckState(QtCore.Qt.PartiallyChecked),
                    50,
                    item.qty)

            else:
                addLineEdit(self,
                            item.historical and (item.expended is not None and
                                                 str(item.expended) or
                                                 ''),
                            rbase, 4, 2, 1)
                self._layout.addItem(QtGui.QSpacerItem(0, 0), rbase, 5, 2, -1)

    @Slot()
    def addBlank(self):
        """Add a blank row to the end of the grid"""
        base = self.moveSpacer()
        name = self.QLineEdit()
        self._layout.addWidget(name, base, 0, 2, 1)
        self._layout.addWidget(self.QLineEdit(), base, 1, 2, 1)
        self._layout.addWidget(self.QLineEdit(), base, 2, 2, 1)
        self._layout.addItem(QtGui.QSpacerItem(0, 0), base, 3, 2, 2)
        for j in range(2):
            for i in range(25):
                self._layout.addWidget(QtGui.QCheckBox(),
                                       (base+j), (5+i+(i//5)))
            for i in range(4):
                self._layout.addItem(QtGui.QSpacerItem(10, 10),
                                     (base+j), (i*6+10))

        def initNewCharge(c, x):
            c.setObjectName('new_charge_%d' % (x))
            c.setTristate(True)
            c.setCheckState(QtCore.Qt.PartiallyChecked)
            c.clicked.connect(self.chargeClicked)
        self.capply(base, initNewCharge)

        # move slot attachment to new row
        if self._newnamefield is not None:
            self._newnamefield.editingFinished.disconnect(self.addBlank)
        name.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp('.+')))
        name.editingFinished.connect(self.addBlank)
        self._newnamefield = name

    @Slot(int)
    def chargeClicked(self, checked=None):
        """A charge checkbox was clicked"""
        c = self.sender()
        (ibase, _, _, _) = self._layout.getItemPosition(self._layout.indexOf(c))
        base = ibase - ibase % 2
        m = re.match(r'new_charge_(\d+)', c.objectName())
        if m:
            x = int(m.group(1))
            self.capply(
                base,
                lambda c, x: c.setCheckState(QtCore.Qt.Unchecked),
                x+1)
            self.capply(
                base,
                lambda c, x: c.setCheckState(QtCore.Qt.PartiallyChecked),
                50,
                x+1)
        m = re.match(r'charge_(\d+)', c.objectName())
        if m:
            x = int(m.group(1))
            item = self.inventory[ibase//2-1]
            self.capply(
                base,
                lambda c, x: c.setCheckState(QtCore.Qt.Checked),
                x+1,
                item.qty - item.remain)
            self.capply(
                base,
                lambda c, x: c.setCheckState(QtCore.Qt.Unchecked),
                item.qty,
                x+1)
            self.capply(
                base,
                lambda c, x: c.setCheckState(QtCore.Qt.PartiallyChecked),
                50,
                item.qty)
        self.dirty.emit()

    @Slot()
    def flush(self, inventory):
        """Flush the grid state to the inventory"""
        if self.inventory is None:
            # first flush of new char
            self._inventory = inventory
        if inventory != self.inventory:
            return

        # existing items - check sold/expended/charges
        base = 2
        for item in self.inventory:
            name = self._layout.itemAtPosition(base, 0).widget().text()
            w = self._layout.itemAtPosition(base, 3).widget()
            # check for sold
            if w.isEnabled() and w.text():
                item.sold = int(w.text())
            if isinstance(item, Item):
                # check for expended
                w = self._layout.itemAtPosition(base, 4).widget()
                if w.isEnabled() and w.text():
                    item.expended = int(w.text())
            elif isinstance(item, Consumable):
                # check for charges expended
                c = self.ccount(base)[QtCore.Qt.Checked]
                c = c - item.qty + item.remain
                item -= c
                self.capply(
                    base,
                    lambda c, x: c.setCheckState(QtCore.Qt.PartiallyChecked),
                    item.qty - item.remain)
                self.capply(
                    base,
                    lambda c, x: c.setEnabled(False),
                    item.qty - item.remain)
            # if now historical, disable controls
            if item.historical:
                for i in range(3, 5):
                    self._layout.itemAtPosition(base, i).widget() \
                        .setEnabled(False)
                if isinstance(item, Consumable):
                    self._layout.itemAtPosition(base, 4).widget() \
                        .setChecked(True)
            base = base + 2

        # new items
        while base < self._layout.rowCount()-1:
            name = self._layout.itemAtPosition(base, 0).widget().text()
            if name:
                cost = re.subn(
                    '[pP]', '',
                    self._layout.itemAtPosition(base, 1).widget().text())
                try:
                    cost = (int(cost[0]), cost[1])
                except ValueError:
                    cost = (float(cost[0]), cost[1])
                bought = int(
                    self._layout.itemAtPosition(base, 2).widget().text())
                c = self._layout.itemAtPosition(base, 5).widget()
                if c.checkState() == QtCore.Qt.PartiallyChecked:
                    item = Item(name, cost, bought)
                else:
                    item = Consumable(name, cost, bought,
                                      self.ccount(base)[QtCore.Qt.Unchecked])
                inventory += item
                # reset row to existing item mode
                for i in range(3):
                    self._layout.itemAtPosition(base, i).widget() \
                        .setEnabled(False)
                self._layout.removeItem(self._layout.itemAtPosition(base, 3))
                w = QtGui.QLineEdit()
                self._layout.addWidget(w, base, 3, 2, 1)
                if isinstance(item, Item):
                    w = QtGui.QLineEdit()
                    self._layout.addWidget(w, base, 4, 2, 1)
                    for j in range(2):
                        for i in range(25+4):
                            w = self._layout.itemAtPosition(base+j, 5+i)
                            if w.widget():
                                w.widget().deleteLater()
                            self._layout.removeItem(w)
                    self._layout.addItem(QtGui.QSpacerItem(0, 0),
                                         base, 5, 2, -1)
                elif isinstance(item, Consumable):
                    self._layout.addWidget(QtGui.QCheckBox(), base, 4, 2, 1)
                    self.capply(
                        base,
                        lambda c, x: c.setObjectName('charge_%d' % (x)))
                    self.capply(
                        base,
                        lambda c, x: c.setCheckState(QtCore.Qt.Unchecked),
                        item.qty)
                    self.capply(
                        base,
                        lambda c, x: c.setEnabled(False),
                        50, item.qty)
            base = base + 2


class CharacterPage(AbstractInventoryWidget):
    """The character page"""
    dirtyChanged = Signal(bool)
    closed = Signal()
    nameChanged = Signal(str)
    beforeSave = Signal(Inventory)

    def __init__(self, filename=None, parent=None):
        self._filename = filename
        inventory = filename and Inventory.load(filename)
        super(CharacterPage, self).__init__(inventory, parent)
        self._dirty = False
        self._layout = QtGui.QVBoxLayout()
        self._details = CharacterDetailsWidget(inventory, self)
        self._grid = InventoryGridWidget(inventory, self)
        self._layout.addWidget(self._details)
        scroll = QtGui.QScrollArea()
        scroll.setWidgetResizable(True)
        scroll.setWidget(self._grid)
        self._layout.addWidget(scroll, stretch=1)
        self.setLayout(self._layout)
        if self.inventory is None:
            self._details.characterDetailsChanged.connect(self.chardetails)
            self._details.characterNameChanged.connect(self.nameChanged)
        self._grid.dirty.connect(self.griddirty)

    @property
    def name(self):
        return (self.inventory is not None and
                self.inventory.character_name or
                self._details.name or
                None)

    @property
    def dirty(self):
        return self._dirty

    def _setDirty(self, dirty=True):
        self._dirty = dirty
        self.dirtyChanged.emit(dirty)

    def _setClean(self):
        self._setDirty(False)

    @Slot()
    def save(self):
        if self._filename and self._inventory is not None:
            self.beforeSave.emit(self._inventory)
            self._inventory.save(self._filename)
            self._setClean()
        else:
            self.saveas()

    @Slot()
    def saveas(self):
        filename, _ = QtGui.QFileDialog.getSaveFileName(
            self, "Save Inventory Sheet",
            (self._filename or (self.name or 'inventory')+'.its'),
            "Inventory Files (*.its)")
        if filename:
            self._filename = filename
            if self._inventory is None:
                self._inventory = Inventory(**self._details.details)
            self.save()

    @Slot()
    def revert(self):
        pass  # FIXME: implement

    @Slot()
    def close(self):
        if self.dirty:
            ret = QtGui.QMessageBox.warning(
                self, "PFS Inventory",
                ("The inventory has been modified.\n" +
                 "Do you want to save your changes?"),
                (QtGui.QMessageBox.Save |
                 QtGui.QMessageBox.Discard |
                 QtGui.QMessageBox.Cancel),
                QtGui.QMessageBox.Save)
            if ret == QtGui.QMessageBox.Cancel:
                return False
            if ret == QtGui.QMessageBox.Save:
                self.save()
        self.closed.emit()

    @Slot()
    def export(self):
        # ensure inventory is clean
        if self.dirty or self._inventory is None or not self._filename:
            ret = QtGui.QMessageBox.warning(
                self, "PFS Inventory",
                ("The inventory has been modified.\n" +
                 "Do you want to save your changes?"),
                (QtGui.QMessageBox.Save |
                 QtGui.QMessageBox.Cancel),
                QtGui.QMessageBox.Save)
            if ret == QtGui.QMessageBox.Cancel:
                return False
            if ret == QtGui.QMessageBox.Save:
                self.save()

        # find available output modules
        from ..output import renderers
        modules = {}
        for r in renderers():
            t = '%s (%s)' % (r.__doc__,
                             ' '.join('*.%s' % e for e in r.extensions()))
            modules[t] = r
        ext = modules[next((m for m in modules))].default_extension()

        # show dialog
        # TODO: change 'Save' button to 'Export'?
        filename, filt = QtGui.QFileDialog.getSaveFileName(
            self, "Export Inventory Sheet",
            (self._filename and
             (os.path.splitext(self._filename)[0]+'.'+ext) or
             (self.name or 'inventory')+ext),
            ';;'.join(m for m in modules))

        # perform output
        if filename:
            with modules[filt](filename) as out:
                # TODO: output module options?
                out.render(self.inventory)

    @Slot(str)
    def chardetails(self, val):
        self._setDirty()

    @Slot()
    def griddirty(self):
        self._setDirty()


class DocumentTabs(QtGui.QTabWidget):
    """The main document area of the application"""
    titleChanged = Signal(str)

    def __init__(self, parent=None):
        super(DocumentTabs, self).__init__(parent)
        self.setDocumentMode(True)
        self.setTabsClosable(True)
        self.initSignals()
        self._untitled = 0
        self._currentPage = None

    def initSignals(self):
        from . import actions
        actions.new.triggered.connect(self.new)
        actions.open.triggered.connect(self.open)
        self.currentChanged.connect(self.tabChanged)
        self.tabCloseRequested.connect(self.tabClosing)

    def addTab(self, page, title):
        super(DocumentTabs, self).addTab(page, title)
        self.setCurrentWidget(page)
        # signals always connected to the page
        from . import actions
        actions.saveall.triggered.connect(page.save)
        actions.closeall.triggered.connect(page.close)
        # signals from the page
        page.closed.connect(self.tabClosed)
        page.nameChanged.connect(self.nameChanged)
        page.dirtyChanged.connect(self.dirtyChanged)

    @Slot()
    def new(self):
        newpage = CharacterPage()
        newname = '*untitled'
        if self._untitled:
            newname = newname + ' ' + str(self._untitled+1)
        self._untitled = self._untitled + 1
        self.addTab(newpage, newname)
        newpage._details._playername.setFocus()  # TODO: clean up

    @Slot()
    def open(self, filename=None):
        # get filename if none provided
        if filename is None:
            filename, _ = QtGui.QFileDialog.getOpenFileName(
                self, "Open Inventory Sheet",
                None, "Inventory Files (*.its)")
            if not filename:
                return

        # if the first page opened, close any untouched new page
        if (self._untitled == 1 and self.count() == 1 and
                self.widget(0).name is None and not self.widget(0).dirty):
            self.widget(0).close()

        # open the file
        newpage = CharacterPage(filename)
        self.addTab(newpage, newpage.name)
        newpage.setFocus()

    @Slot(int)
    def tabClosing(self, idx):
        self.widget(idx).close()

    @Slot()
    def tabClosed(self):
        page = self.sender()
        self.removeTab(self.indexOf(page))
        page.deleteLater()

    @Slot(int)
    def tabChanged(self, idx):
        # manage signals that need to be connected to current page only
        from . import actions
        self._reattach(idx, actions.save.triggered, 'save')
        self._reattach(idx, actions.saveas.triggered, 'saveas')
        self._reattach(idx, actions.revert.triggered, 'revert')
        self._reattach(idx, actions.close.triggered, 'close')
        self._reattach(idx, actions.export.triggered, 'export')
        self.titleChanged.emit(self.tabText(idx))
        self._currentPage = self.widget(idx)

    def _reattach(self, idx, signal, slotname):
        newPage = self.widget(idx)
        if newPage == self._currentPage:
            return
        if self._currentPage:
            signal.disconnect(getattr(self._currentPage, slotname))
        if newPage:
            signal.connect(getattr(newPage, slotname))

    def _tabText(self, page):
        return (page.dirty and '*' or '') + page.name

    @Slot(bool)
    def dirtyChanged(self, dirty):
        page = self.sender()
        if not page.name:
            return
        self.setTabText(self.indexOf(page), self._tabText(page))
        if page == self._currentPage:
            self.titleChanged.emit(self.tabText(self.indexOf(page)))

    @Slot(str)
    def nameChanged(self, name):
        page = self.sender()
        self.setTabText(self.indexOf(page), self._tabText(page))
        if page == self._currentPage:
            self.titleChanged.emit(self.tabText(self.indexOf(page)))
