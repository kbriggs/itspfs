# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    Main window for the itspfs GUI
"""

import sys

from PySide import QtGui
from PySide.QtCore import Slot

from itspfs.gui import views


class MainWindow(QtGui.QMainWindow):
    def __init__(self, **kwargs):
        super(MainWindow, self).__init__()
        self.setWindowTitle('PFS Inventory')
        self.statusBar()
        self.initMenu()
        self.initToolbar()

        # the main content
        tabs = views.DocumentTabs()
        self.setCentralWidget(tabs)
        tabs.titleChanged.connect(self.title)

        # load initial documents
        if 'files' in kwargs and kwargs['files']:
            for f in kwargs['files']:
                tabs.open(f)
        else:
            tabs.new()

    def initMenu(self):
        from . import actions
        fileMenu = self.menuBar().addMenu("&File")
        fileMenu.addAction(actions.new)
        fileMenu.addAction(actions.open)
        fileMenu.addSeparator()
        fileMenu.addAction(actions.save)
        fileMenu.addAction(actions.saveas)
        fileMenu.addAction(actions.export)
        fileMenu.addAction(actions.revert)
        fileMenu.addSeparator()
        fileMenu.addAction(actions.close)
        actions.exit.triggered.connect(self.close)
        fileMenu.addAction(actions.exit)

        documentsMenu = self.menuBar().addMenu("&Documents")
        documentsMenu.addAction(actions.saveall)
        documentsMenu.addAction(actions.closeall)
        documentsMenu.addSeparator()

        helpMenu = self.menuBar().addMenu("&Help")
        actions.about.triggered.connect(self.about)
        helpMenu.addAction(actions.about)
        actions.license.triggered.connect(self.license)
        helpMenu.addAction(actions.license)

    def initToolbar(self):
        from . import actions
        fileToolbar = self.addToolBar("File")
        fileToolbar.addAction(actions.new)
        fileToolbar.addAction(actions.open)
        fileToolbar.addAction(actions.save)
        fileToolbar.addAction(actions.saveas)
        fileToolbar.addAction(actions.export)

    @Slot()
    def about(self):
        """Show about dialog"""
        import platform
        import PySide
        QtGui.QMessageBox.about(
            self, "About",
            """<p><b>Pathfinder Society Inventory Tracking System</b>
            <p>Copyright &copy; 2014 Kieron Briggs</p>
            <p><small>(debug: %s %s %s %s)</small></p>""" % (
                platform.python_version(),
                PySide.__version__,
                PySide.QtCore.__version__,
                platform.system()))

    @Slot()
    def license(self):
        """Show license dialog"""
        from .license import LicenseDialog
        dialog = LicenseDialog(self)
        dialog.exec_()
        dialog.deleteLater()

    @Slot(str)
    def title(self, text):
        self.setWindowTitle((text and text + ' - ') + 'PFS Inventory')


class MainApplication(QtGui.QApplication):
    def __init__(self, args):
        super(MainApplication, self).__init__(args)

        # load icon theme
        if not QtGui.QIcon.themeName():
            from PySide.QtCore import QResource
            from pkg_resources import resource_filename
            QResource.registerResource(
                resource_filename(__name__, 'tango.rcc'))
            QtGui.QIcon.setThemeName('tango')

        self.args = self.parse_args(args)

        # check for export
        if (self.is_export()):
            sys.exit(self.export())

        self.window = MainWindow(**vars(self.args))
        self.window.show()

    def parse_args(self, args):
        from argparse import ArgumentParser
        parser = ArgumentParser(prog='itspfs',
                                description=(
                                    'Inventory tracking system for ' +
                                    'Pathfinder Society organised play.'),
                                )
        self.argparser = parser

        # arguments for export
        parser.add_argument('-e', '--export', metavar='format',
                            help=(
                                'export file to format, instead of ' +
                                'opening it')
                            )
        parser.add_argument('-o', '--output', metavar='file',
                            help=(
                                'output filename for export, instead ' +
                                'of deriving from the input filename ' +
                                'and format')
                            )
        parser.add_argument('-l', '--list-formats', action='store_true',
                            help=(
                                'list available export formats')
                            )

        # filenames to open / export / etc
        parser.add_argument('files', nargs='*', metavar='file',
                            help='%(prog)s file to open')

        return parser.parse_args(self.real_arguments(args))

    def real_arguments(self, args):
        """Return the actual arguments (i.e. args[1:] with caveats)"""
        import os
        if os.name == 'nt':
            # See note on QApplication::arguments() re: NT-based windows
            # self.arguments() still has args that would be removed by python interpretter (e.g. python -m);
            # sys.args still has args that would be removed by Qt (e.g. -style)
            qtargs = self.arguments()
            return (x for x in args[1:] if x in qtargs)
        else:
            return self.arguments()[1:]

    def is_export(self):
        return self.args.list_formats or self.args.export

    def export(self):
        from ..inventory import Inventory
        from ..output import renderers

        def _n(name):
            import re
            return re.sub(r'^itspfs.output.', '', name)

        # list formats
        if self.args.list_formats:
            for r in renderers():
                print('%s\t%s' % (_n(r.__module__), r.__doc__))
            return 0

        # check number of input files
        if len(self.args.files) == 0:
            self.argparser.error('at least one input file must be ' +
                                 'specified for export')
        if self.args.output and len(self.args.files) != 1:
            self.argparser.error('only one input file allowed with ' +
                                 'explicit output name')

        # find requested renderer
        renderer = None
        for r in renderers():
            if (self.args.export == r.__module__ or
                    self.args.export == _n(r.__module__)):
                renderer = r
        if not renderer:
            self.argparser.error('invalid export type %s' %
                                 (self.args.export))

        for f in self.args.files:
            def _o(name):
                from os import path, extsep
                return (path.splitext(name)[0] + extsep +
                        renderer.default_extension())
            inv = Inventory.load(f)
            with renderer(self.args.output or _o(f)) as out:
                out.render(inv)


def main():
    app = MainApplication(sys.argv)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
