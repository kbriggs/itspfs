# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    Top-level actions (open, save, etc) for menu and tool bars
"""

from PySide import QtGui

# New inventory sheet
new = QtGui.QAction(QtGui.QIcon.fromTheme('document-new'), '&New', None)
new.setShortcut('Ctrl+N')
new.setStatusTip('New inventory sheet')

# Open inventory sheet
open = QtGui.QAction(QtGui.QIcon.fromTheme('document-open'), '&Open...', None)
open.setShortcut('Ctrl+O')
open.setStatusTip('Open inventory sheet')

# Save inventory sheet
save = QtGui.QAction(QtGui.QIcon.fromTheme('document-save'), '&Save', None)
save.setShortcut('Ctrl+S')
save.setStatusTip('Save inventory sheet')

# Save inventory sheet as...
saveas = QtGui.QAction(QtGui.QIcon.fromTheme('document-save-as'),
                       'Save &As...', None)
saveas.setShortcut('Shift+Ctrl+S')
saveas.setStatusTip('Save inventory sheet as...')

# Save all inventory sheets
saveall = QtGui.QAction(QtGui.QIcon.fromTheme('document-save'),
                        '&Save All', None)
saveall.setShortcut('Shift+Ctrl+L')
saveall.setStatusTip('Save all inventory sheets')

# Revert to saved
revert = QtGui.QAction(QtGui.QIcon.fromTheme('document-revert'),
                       '&Revert', None)
revert.setStatusTip('Revert to saved')

# Close inventory sheet
close = QtGui.QAction(QtGui.QIcon.fromTheme('window-close'), '&Close', None)
close.setShortcut('Ctrl+W')
close.setStatusTip('Close inventory sheet')

# Close all inventory sheets
closeall = QtGui.QAction(QtGui.QIcon.fromTheme('window-close'),
                         '&Close All', None)
closeall.setShortcut('Shift+Ctrl+W')
closeall.setStatusTip('Close all inventory sheets')

# Export inventory sheet
export = QtGui.QAction(QtGui.QIcon.fromTheme('document-export'),
                       '&Export...', None)
export.setShortcut('Ctrl+E')
export.setStatusTip('Export inventory sheet')

# Exit
exit = QtGui.QAction(QtGui.QIcon.fromTheme('application-exit'), '&Quit', None)
exit.setShortcut('Ctrl+Q')
exit.setStatusTip('Exit application')

# About
about = QtGui.QAction(QtGui.QIcon.fromTheme('help-about'), '&About...', None)
about.setStatusTip('About this application')

# Show license
license = QtGui.QAction(QtGui.QIcon.fromTheme('help-license'),
                        '&License...', None)
license.setStatusTip('License information')
