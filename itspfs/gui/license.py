# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    License dialog for itspfs GUI
"""

from PySide import QtGui
from PySide.QtCore import Qt


COMMUNITY_USE_HTML = ' '.join([
    'This program uses trademarks and/or copyrights owned by Paizo',
    'Inc., which are used under Paizo\'s Community Use Policy. We',
    'are expressly prohibited from charging you to use or access',
    'this content. This program is not published, endorsed, or',
    'specifically approved by Paizo Inc. For more information about',
    'Paizo\'s Community Use Policy, please visit',
    '<a href="http://paizo.com/communityuse">paizo.com/communityuse</a>.',
    'For more information about Paizo Inc. and Paizo products,',
    'please visit <a href="http://paizo.com">paizo.com</a>.'])


class LicenseDialog(QtGui.QDialog):
    """Dialog box that displays the legal stuff"""

    def __init__(self, parent=None):
        super(LicenseDialog, self).__init__(parent)
        self.setWindowTitle("License Agreement")
        self.layout = QtGui.QVBoxLayout()

        self.layout.addWidget(self.create_gpl_widget(), stretch=1)
        self.layout.addWidget(self.create_cpl_widget())

        buttons = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        buttons.accepted.connect(self.accept)
        self.layout.addWidget(buttons)

        self.setLayout(self.layout)

    def create_gpl_widget(self):
        text = QtGui.QTextEdit(self)
        monospace = QtGui.QFont("Courier", 8)
        monospace.setStyleHint(QtGui.QFont.Monospace)
        text.setFont(monospace)
        data = get_gplv3()
        if data is not None:
            text.setText(data)
        text.setReadOnly(True)
        text.setWordWrapMode(QtGui.QTextOption.NoWrap)
        text.setMinimumWidth(
            QtGui.QFontMetrics(text.currentFont()).maxWidth() * 82)
        return text

    def create_cpl_widget(self):
        cpl = QtGui.QLabel(COMMUNITY_USE_HTML, self)
        cpl.setWordWrap(True)
        cpl.setOpenExternalLinks(True)
        cpl.setTextInteractionFlags(Qt.LinksAccessibleByMouse |
                                    Qt.LinksAccessibleByKeyboard)
        cpl.setAlignment(Qt.AlignCenter)
        return cpl


def get_gplv3():
    """Find the full GPLv3 text"""

    data = None
    try:
        import pkg_resources
        data = pkg_resources.resource_string('itspfs', 'COPYING.txt')
    except IOError:
        try:
            data = open('COPYING.txt').read()
        except IOError:
            try:
                from six.moves.urllib.request import urlopen
                from contextlib import closing
                with closing(urlopen(
                             'http://www.gnu.org/licenses/gpl-3.0.txt'
                             )) as f:
                    data = f.read().decode('ascii')
            except IOError:
                pass
    return data
