An inventory tracking system for Pathfinder Society.

.. note::

   This program uses trademarks and/or copyrights owned by Paizo Inc.,
   which are used under Paizo's Community Use Policy. We are expressly
   prohibited from charging you to use or access this content. This
   program is not published, endorsed, or specifically approved by Paizo
   Inc. For more information about Paizo's Community Use Policy, please
   visit `paizo.com/communityuse <http://paizo.com/communityuse>`_. For
   more information about Paizo Inc. and Paizo products, please visit
   `paizo.com <http://paizo.com>`_.
