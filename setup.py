# -*- coding: utf-8 -*-

# Copyright © 2014 Kieron Briggs
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
    Configuration for setuptools
"""

import os
from setuptools import setup, find_packages, Command
from distutils.command.build import build as _build


def readme():
    with open('README.rst') as f:
        return f.read()


class build_tango(Command):
    description = "build the tango icon set resource"

    user_options = [
        ('build-lib=', 'd', "directory to build to"),
        ('build-temp=', 't',
         "directory for temporary files (build by-products)"),
        ('force', 'f',
         "forcibly build everything (ignore file timestamps)"),
        ]

    boolean_options = ['force']

    base_url = 'http://tango.freedesktop.org/releases/'
    tango_version = '0.8.90'

    def initialize_options(self):
        self.build_lib = None
        self.build_temp = None
        self.force = None

    def finalize_options(self):
        self.set_undefined_options('build',
                                   ('build_lib', 'build_lib'),
                                   ('build_temp', 'build_temp'),
                                   ('force', 'force'))

    def run(self):
        def theme_files(members):
            l = len('tango-icon-theme-%s' % self.tango_version) + 1
            for tarinfo in members:
                if os.path.splitext(tarinfo.name)[1] == '.png':
                    self.files.append(tarinfo.name[l:])
                    yield tarinfo
                elif os.path.basename(tarinfo.name) == 'index.theme.in':
                    self.files.append((tarinfo.name[l:], 'index.theme'))
                    yield tarinfo
                elif os.path.basename(tarinfo.name) in ('AUTHORS',
                                                        'COPYING',
                                                        'ChangeLog'):
                    self.files.append(tarinfo.name[l:])
                    yield tarinfo

        build_dir = os.path.join(
            self.build_lib,
            'itspfs', 'gui')
        self.mkpath(build_dir)
        target = os.path.join(build_dir, 'tango.rcc')
        if os.path.isfile(target) and not self.force:
            self.announce('not compiling %s (already exists)' %
                          os.path.basename(target))
            return

        # extract tango icon theme to temporary directory
        from six.moves.urllib.request import urlopen
        from tarfile import open as taropen
        from contextlib import closing
        temp_dir = os.path.join(
            self.build_temp,
            'tango-icon-theme-%s' % self.tango_version)
        src = ('%stango-icon-theme-%s.tar.bz2' %
               (self.base_url, self.tango_version))
        self.announce('extracting %s to %s' % (src, temp_dir))
        self.files = []
        with closing(urlopen(src)) as s:
            with taropen(None, 'r|*', s) as tar:
                tar.extractall(self.build_temp, members=theme_files(tar))

        # generate qrc file
        import xml.etree.cElementTree as et
        qrc = os.path.join(temp_dir, 'tango.qrc')
        root = et.Element('RCC', version='1.0')
        e = et.SubElement(root, 'qresource', prefix='/icons/tango')
        for f in self.files:
            if isinstance(f, tuple):
                et.SubElement(e, 'file', alias=f[1]).text = f[0]
            else:
                et.SubElement(e, 'file').text = f
        et.ElementTree(root).write(qrc)

        # and compile the binary
        from subprocess import check_call as call
        call(['rcc', '-binary', '-o', target, qrc])


class build_copying(Command):
    description = "custom command to copy in the COPYING.txt file"

    user_options = [
        ('build-lib=', 'd', "directory to \"build\" (copy) to"),
        ('force', 'f', "forcibly build everything (ignore file timestamps)"),
        ]

    boolean_options = ['force']

    filenames = [base + ext
                 for base in ['COPYING', 'LICENSE']
                 for ext in ['', '.txt', '.rst', '.md']]

    def initialize_options(self):
        self.build_lib = None
        self.filename = None

    def finalize_options(self):
        self.set_undefined_options('build',
                                   ('build_lib', 'build_lib'),
                                   ('force', 'force'))

        if self.filename is None:
            self.filename = next(
                (f for f in self.filenames if os.path.isfile(f)),
                None)
        if self.filename is not None:
            self.announce('Found %s' % self.filename)

    def run(self):
        if self.filename is not None:
            build_dir = os.path.join(*(
                [self.build_lib] +
                self.distribution.packages[0].split('.')))
            self.mkpath(build_dir)
            self.copy_file(self.filename,
                           os.path.join(build_dir, self.filename))


def has_copying(cmd):
    cmd.announce('d %s' % os.getcwd())
    return (
        next((f for f in build_copying.filenames if os.path.isfile(f)), None)
        is not None)

# monkey-patch into build subcommands
_build.sub_commands.extend([
    ('build_cp', has_copying),
    ('build_tango', None)
    ])


setup(
    name='itspfs',
    version='0.1.0',
    description='An inventory tracking system for Pathfinder Society',
    long_description=readme(),
    keywords='itspfs',
    author='Kieron Briggs',
    author_email='kbriggs@gmail.com',
    url='https://gitlab.com/kbriggs/itspfs',
    license='GPLv3+',
    install_requires=[
        'PySide >= 1.2.2',
        'six',
        ],
    setup_requires=[
        'setuptools_git >= 0.3',
        'six',
        ],
    packages=find_packages(exclude=['tests', 'tests.*']),
    entry_points={
        'console_scripts': [
            ],
        'gui_scripts': [
            'itspfs = itspfs.gui.main:main',
            ],
        },
    test_suite='tests',
    include_package_data=True,
    classifiers=[
        'Private :: Do not upload',
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Other Environment',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Topic :: Games/Entertainment :: Role-Playing',
        ],
    cmdclass={
        'build_cp': build_copying,
        'build_tango': build_tango,
        }
)
