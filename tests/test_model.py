import unittest
from itspfs import inventory

# TODO: refactor test object / value initialisation to reduce duplication


class TestInventory(unittest.TestCase):
    def test_inv_empty(self):
        inv = inventory.Inventory()
        self.assertIsNone(inv.character_name)
        self.assertIsNone(inv.player_name)
        self.assertIsNone(inv.faction)
        self.assertEqual(inv.pfs_id, (None, None))
        # TODO: assert inventory is empty of items

    def test_inv_char(self):
        args = {'player': 'Fred', 'name': 'Killmaster', 'faction': 'Grand Lodge', 'pfs': '#123456-7'}
        inv = inventory.Inventory(**args)
        self.assertEqual(inv.character_name, args['name'])
        self.assertEqual(inv.player_name, args['player'])
        self.assertEqual(inv.faction, args['faction'])
        self.assertEqual(inv.pfs_id, (123456, 7))

    def test_inv_item(self):
        inv = inventory.Inventory(player='Fred', name='Killmaster', pfs=(123456, 7))
        self.assertEqual(len(inv), 0)
        item1 = inventory.Item('Sword of swordiness', 150, 1)
        item2 = inventory.Item('Bag of holding', 10000, 5)
        item3 = inventory.Item('Ten foot pole', 0.5, 3)
        inv += item1
        inv += [item2, item3]
        self.assertEqual(len(inv), 3)
        self.assertEqual(inv[0], item1)
        self.assertEqual(inv[1], item2)
        self.assertEqual(inv[2], item3)
        self.assertEqual(inv.character_name, 'Killmaster')

    def test_inv_iter(self):
        inv = inventory.Inventory(player='Fred', name='Killmaster', pfs=(123456, 7))
        self.assertEqual(len(inv), 0)
        item1 = inventory.Item('Sword of swordiness', 150, 1)
        item2 = inventory.Item('Bag of holding', 10000, 5)
        item3 = inventory.Item('Ten foot pole', 0.5, 3)
        inv += item1
        inv += [item2, item3]
        self.assertEqual(len(inv), 3)
        i = 0
        for item in inv:
            self.assertEqual(item, (item1, item2, item3)[i])
            i += 1

    def test_item(self):
        item = inventory.Item('Sword of swordiness', 150, 1)
        self.assertEqual(item.name, 'Sword of swordiness')
        self.assertEqual(item.cost, 150)
        self.assertEqual(item.bought, 1)
        self.assertIsNone(item.sold)
        self.assertIsNone(item.expended)
        item.sold = 3
        self.assertEqual(item.sold, 3)

    def test_consumable(self):
        item = inventory.Consumable('Arrows of pointiness', 1, 3, 20)
        self.assertEqual(item.name, 'Arrows of pointiness')
        self.assertEqual(item.cost, 1)
        self.assertEqual(item.bought, 3)
        self.assertEqual(item.qty, 20)
        self.assertEqual(item.remain, 20)
        self.assertIsNone(item.sold)
        item -= 3
        self.assertEqual(item.qty, 20)
        self.assertEqual(item.remain, 17)

    def test_item_prestige(self):
        item = inventory.Consumable('Wand of prestigiousness', (0, 2), 2, 50)
        self.assertEqual(item.name, 'Wand of prestigiousness')
        self.assertEqual(item.cost, (0, 2))

    def test_json_item(self):
        import json
        item1 = inventory.Item('Sword of swordiness', 150, 1)
        self.assertEqual(json.dumps(item1, cls=inventory.Item._JSONEncoder, sort_keys=True),
                         '{"bought": 1, "cost": 150, "name": "Sword of swordiness"}')
        item2 = inventory.Consumable('Arrows of pointiness', 1, 3, 20)
        item2 -= 3
        self.assertEqual(json.dumps(item2, cls=inventory.Item._JSONEncoder, sort_keys=True),
                         '{"bought": 3, "cost": 1, "name": "Arrows of pointiness", "qty": 20, "remain": 17}')

    def test_json_item_prestige(self):
        import json
        item = inventory.Consumable('Wand of prestigiousness', (0, 2), 2, 50)
        self.assertEqual(json.dumps(item, cls=inventory.Item._JSONEncoder, sort_keys=True),
                         '{"bought": 2, "cost": [0, 2], "name": "Wand of prestigiousness", "qty": 50, "remain": 50}')

    def test_inv_save_stream(self):
        from io import StringIO
        from textwrap import dedent
        inv = inventory.Inventory(player='Fred', name='Killmaster', pfs=(123456, 7))
        self.assertEqual(len(inv), 0)
        item1 = inventory.Item('Sword of swordiness', 150, 1)
        item2 = inventory.Consumable('Arrows of pointiness', 1, 3, 20)
        item2 -= 3
        inv += [item1, item2]
        with StringIO() as s:
            inv.save(s)
            self.assertEqual(s.getvalue(), dedent('''\
                    {"name": "Killmaster", "pfs": "123456-7", "player": "Fred", "inv": [
                      {"bought": 1, "cost": 150, "name": "Sword of swordiness"}
                    , {"bought": 3, "cost": 1, "name": "Arrows of pointiness", "qty": 20, "remain": 17}
                    ]}
                '''))

    def test_inv_save_file(self):
        import os
        from tempfile import NamedTemporaryFile
        from textwrap import dedent
        inv = inventory.Inventory(player='Fred', name='Killmaster', pfs=(123456, 7))
        self.assertEqual(len(inv), 0)
        item1 = inventory.Item('Sword of swordiness', 150, 1)
        item2 = inventory.Consumable('Arrows of pointiness', 1, 3, 20)
        item2 -= 3
        inv += [item1, item2]
        try:
            filename = None
            with NamedTemporaryFile(delete=False) as tmp:
                filename = tmp.name
            inv.save(filename)
            with open(filename, 'r') as f:
                self.assertEqual(f.read(), dedent(u'''\
                        {"name": "Killmaster", "pfs": "123456-7", "player": "Fred", "inv": [
                          {"bought": 1, "cost": 150, "name": "Sword of swordiness"}
                        , {"bought": 3, "cost": 1, "name": "Arrows of pointiness", "qty": 20, "remain": 17}
                        ]}
                    '''))
        finally:
            if filename:
                os.remove(filename)

    def test_inv_load_stream(self):
        from io import StringIO
        from textwrap import dedent
        text = dedent(u'''\
                {"name": "Killmaster", "pfs": "123456-7", "player": "Fred", "inv": [
                  {"bought": 1, "cost": 150, "name": "Sword of swordiness"}
                , {"bought": 3, "cost": 1, "name": "Arrows of pointiness", "qty": 20, "remain": 17}
                ]}
            ''')
        with StringIO(text) as f:
            inv = inventory.Inventory.load(f)
        self.assertEqual(inv.character_name, 'Killmaster')
        self.assertEqual(inv.player_name, 'Fred')
        self.assertIsNone(inv.faction)
        self.assertEqual(inv.pfs_id, (123456, 7))
        self.assertEqual(len(inv), 2)
        items = [inventory.Item('Sword of swordiness', 150, 1),
                 inventory.Consumable('Arrows of pointiness', 1, 3, 20)]
        items[1] -= 3
        i = 0
        for item in inv:
            self.assertEqual(item, items[i])
            i += 1

    def test_inv_load_string(self):
        from textwrap import dedent
        text = dedent('''\
                {"name": "Killmaster", "pfs": "123456-7", "player": "Fred", "inv": [
                  {"bought": 1, "cost": 150, "name": "Sword of swordiness", "sold": 3}
                , {"bought": 3, "cost": 1, "name": "Arrows of pointiness", "qty": 20, "remain": 17}
                ]}
            ''')
        inv = inventory.Inventory.load(text)
        self.assertEqual(inv.character_name, 'Killmaster')
        self.assertEqual(inv.player_name, 'Fred')
        self.assertIsNone(inv.faction)
        self.assertEqual(inv.pfs_id, (123456, 7))
        self.assertEqual(len(inv), 2)
        items = [inventory.Item('Sword of swordiness', 150, 1),
                 inventory.Consumable('Arrows of pointiness', 1, 3, 20)]
        items[0].sold = 3
        items[1] -= 3
        i = 0
        for item in inv:
            self.assertEqual(item, items[i])
            i += 1

    def test_inv_load_file(self):
        import os
        from tempfile import NamedTemporaryFile
        from textwrap import dedent
        try:
            filename = None
            with NamedTemporaryFile(delete=False) as tmp:
                filename = tmp.name
                tmp.write(dedent('''\
                        {"name": "Killmaster", "pfs": "123456-7", "player": "Fred", "inv": [
                          {"bought": 1, "cost": 150, "name": "Sword of swordiness", "expended": 2}
                        , {"bought": 3, "cost": 1, "name": "Arrows of pointiness", "qty": 20, "remain": 17}
                        ]}
                    ''').encode('utf-8'))
            inv = inventory.Inventory.load(filename)
        finally:
            if filename:
                os.remove(filename)
        self.assertEqual(inv.character_name, 'Killmaster')
        self.assertEqual(inv.player_name, 'Fred')
        self.assertIsNone(inv.faction)
        self.assertEqual(inv.pfs_id, (123456, 7))
        self.assertEqual(len(inv), 2)
        items = [inventory.Item('Sword of swordiness', 150, 1),
                 inventory.Consumable('Arrows of pointiness', 1, 3, 20)]
        items[0].expended = 2
        items[1] -= 3
        i = 0
        for item in inv:
            self.assertEqual(item, items[i])
            i += 1

    def test_inv_load_prestige(self):
        from textwrap import dedent
        text = dedent('''\
                {"name": "Killmaster", "pfs": "123456-7", "player": "Fred", "inv": [
                  {"bought": 1, "cost": 150, "name": "Sword of swordiness"}
                , {"bought": 2, "cost": [0, 2], "name": "Wand of prestigiousness", "qty": 50, "remain": 50}
                , {"bought": 3, "cost": 1, "name": "Arrows of pointiness", "qty": 20, "remain": 17}
                ]}
            ''')
        inv = inventory.Inventory.load(text)
        self.assertEqual(inv.character_name, 'Killmaster')
        self.assertEqual(inv.player_name, 'Fred')
        self.assertIsNone(inv.faction)
        self.assertEqual(inv.pfs_id, (123456, 7))
        self.assertEqual(len(inv), 3)
        items = [inventory.Item('Sword of swordiness', 150, 1),
                 inventory.Consumable('Wand of prestigiousness', (0, 2), 2, 50),
                 inventory.Consumable('Arrows of pointiness', 1, 3, 20)]
        items[2] -= 3
        i = 0
        for item in inv:
            self.assertEqual(item, items[i])
            i += 1
