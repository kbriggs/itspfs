# -*- coding: utf-8 -*-

import unittest
from io import StringIO
from itspfs import inventory
from itspfs.output import bbcode, pdf


class TestListRenderers(unittest.TestCase):
    def testListRenderers(self):
        from itspfs.output import renderers
        import six
        six.assertCountEqual(self, tuple(renderers()),
                             (bbcode.BBCodeRenderer,
                              pdf.PdfRenderer,
                              ))


class TestAttribsBBCode(unittest.TestCase):
    def setUp(self):
        self.str = StringIO()
        self.renderer = bbcode.BBCodeRenderer(self.str)

    def testDefaultExtension(self):
        self.assertEqual(self.renderer.default_extension(), 'bbcode')
        self.assertEqual(bbcode.BBCodeRenderer.default_extension(), 'bbcode')

    def testExtensions(self):
        self.assertEqual(self.renderer.extensions(), ('bbcode', 'txt', 'text', 'bbc'))
        self.assertEqual(bbcode.BBCodeRenderer.extensions(), ('bbcode', 'txt', 'text', 'bbc'))

    def testText(self):
        self.assertEqual(self.renderer.text, True)


class FixtureOutput(object):
    def setUp(self):
        self.inv = inventory.Inventory(player='Fred', name='Killmaster', pfs=(123456, 7))
        self.inv += [
            inventory.Item('Sword of swordiness', 150, 1),
            inventory.Consumable('Arrows of pointiness', 1, 3, 20),
            inventory.Item('Shield (human)', 400, 2),
            inventory.Consumable('Wand of Cure Mildly Embarrasing Wounds', 1200, 2, 5),
            inventory.Item('Leather Armour of Phobos', 230, 1),
            inventory.Consumable('Alchemist\'s Deoderant', 20, 3, 5)
        ]
        self.inv[0].expended = 5
        i = self.inv[1]  # TODO figure out why this can't be self.inv[1] -= 3
        i -= 3
        i = self.inv[3]
        i -= 5
        self.inv[4].sold = 4
        i = self.inv[5]
        i -= 2
        i.sold = 4


class TestOutputBBCode(FixtureOutput, unittest.TestCase):
    def testOptions(self):
        opts = list(bbcode.BBCodeRenderer.options())
        self.assertEqual(len(opts), 2)
        self.assertIn(('character_details', bbcode.BBCodeRenderer.character_details), opts)
        self.assertIn(('use_ballots', bbcode.BBCodeRenderer.use_ballots), opts)
        with StringIO() as s:
            r = bbcode.BBCodeRenderer(s)
            self.assertTrue(r.use_ballots)
            self.assertTrue(r.character_details)
            r.character_details = False
            self.assertFalse(r.character_details)

    def testRender(self):
        from textwrap import dedent
        expected = dedent(u"""\
            Player: [b]Fred[/b]
            Character: [b]Killmaster[/b]
            PFS ID: [b]123456-7[/b]

            [spoiler=Inventory]
            [i][b]Item[/b] - Cost Bought/Sold/Expended Charges[/i]
            [b]Arrows of pointiness[/b] - 1 3/-/- ☒☒☒☐☐ ☐☐☐☐☐ ☐☐☐☐☐ ☐☐☐☐☐
            [b]Shield (human)[/b] - 400 2/-/-
            [/spoiler]

            [spolier=Historical Inventory]
            [i][b]Item[/b] - Cost Bought/Sold/Expended Charges[/i]
            [b]Sword of swordiness[/b] - 150 1/-/5
            [b]Wand of Cure Mildly Embarrasing Wounds[/b] - 1200 2/-/x ☒☒☒☒☒
            [b]Leather Armour of Phobos[/b] - 230 1/4/-
            [b]Alchemist's Deoderant[/b] - 20 3/4/- ☒☒☐☐☐
            [/spoiler]
        """)
        with StringIO() as s:
            r = bbcode.BBCodeRenderer(s)
            r.render(self.inv)
            self.assertEqual(s.getvalue(), expected)

    def testRenderPrestige(self):
        self.inv += [
            inventory.Consumable('Wand of Prestige Points', (0, 4), 6, 15)
        ]
        from textwrap import dedent
        expected = dedent(u"""\
            Player: [b]Fred[/b]
            Character: [b]Killmaster[/b]
            PFS ID: [b]123456-7[/b]

            [spoiler=Inventory]
            [i][b]Item[/b] - Cost Bought/Sold/Expended Charges[/i]
            [b]Arrows of pointiness[/b] - 1 3/-/- ☒☒☒☐☐ ☐☐☐☐☐ ☐☐☐☐☐ ☐☐☐☐☐
            [b]Shield (human)[/b] - 400 2/-/-
            [b]Wand of Prestige Points[/b] - 4PP 6/-/- ☐☐☐☐☐ ☐☐☐☐☐ ☐☐☐☐☐
            [/spoiler]

            [spolier=Historical Inventory]
            [i][b]Item[/b] - Cost Bought/Sold/Expended Charges[/i]
            [b]Sword of swordiness[/b] - 150 1/-/5
            [b]Wand of Cure Mildly Embarrasing Wounds[/b] - 1200 2/-/x ☒☒☒☒☒
            [b]Leather Armour of Phobos[/b] - 230 1/4/-
            [b]Alchemist's Deoderant[/b] - 20 3/4/- ☒☒☐☐☐
            [/spoiler]
        """)
        with StringIO() as s:
            r = bbcode.BBCodeRenderer(s)
            r.render(self.inv)
            self.assertEqual(s.getvalue(), expected)
